# AI217-Presentation-GIS


## Installation des dépendances

```bash
pip install -r requirements.txt
```

## QGIS (https://www.qgis.org/fr/site/forusers/download.html)

Les fichiers nécéssaires pour QGIS sont disponibles sur le site du SPF Finances : https://finances.belgium.be/fr/experts-partenaires/donnees-ouvertes-patrimoine/jeux-donnees/portail-telechargement