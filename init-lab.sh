#!/usr/bin/env bash

echo "Warning you'are creating a lab with 10 VM Are you sure to continue ?"
read -p "Press [Enter] key to continue..."

# Create 10 folder name 1 2 3 4 5 and put the content of demo folder in each folder

for i in {1..10}
do
  mkdir $i
  cp -r demo/* $i
done

docker compose up -d