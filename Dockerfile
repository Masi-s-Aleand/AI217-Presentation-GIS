FROM gboeing/osmnx:v1.8.0

RUN jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-leaflet && pip install ipyleaflet ipyflex

WORKDIR /home/jovyan/work

CMD ["jupyter", "lab", "--ip='0.0.0.0'", "--port=8888" ,"--no-browser" ,"--NotebookApp.token=''", "--NotebookApp.password=''"]